<?php

namespace Tests;

use App\Services\ContactService;
use PHPUnit\Framework\TestCase;

class ContactServiceTest extends TestCase
{
	
	/**
	 * @dataProvider cases
	 * @test
	 */
	public function it_validates_phone_numbers($phoneNumber, $expectedValidation)
	{
		$actualValidation = ContactService::validateNumber($phoneNumber);

		$this->assertEquals($expectedValidation, $actualValidation);
	}

	public function cases() {
		return [
			['+51986821895', true],
			['+1986821895', true],
			['', false],
			['9868', false],
			['986821895', false],
			['986821895', false],

			/**  @ReviewerNote */
			// It depends on the technological decision made about supporting this format.
			// Not supporting it in this example.
			['+358 942 720 607', false],
		];
	}

}
