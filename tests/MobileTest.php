<?php

namespace Tests;

use App\Call;
use App\Carriers\TestingCarrier;
use App\Interfaces\CarrierInterface;
use App\Services\ContactService;
use App\Sms;
use Mockery as m;
use PHPUnit\Framework\TestCase;
use App\Mobile;

class MobileTest extends TestCase
{
	
	/** @test */
	public function it_returns_null_when_calling_contact_name_empty()
	{
		// Given a mobile carrier
		$provider = m::mock(CarrierInterface::class);

		// Given a Mobile module
		$mobile = new Mobile($provider);

		// When calling a undefined contact
		$call = $mobile->makeCallByName('');

		// Then the call should not be performed
		$this->assertNull($call, 'Should not perform call');
	}

	/** @test */
	public function it_returns_a_call_instance_when_calling_valid_contact_name()
	{
		// Given an existing contact
		$contactName = 'Cristian Llanos';
		$contact = ContactService::findByName($contactName);

		// Given a mobile carrier
		$provider = m::spy(CarrierInterface::class);
		$expectedCall = new Call($contact);
		$expectedCall->markAsSuccessful();
		$provider->shouldReceive('makeCall')->andReturn($expectedCall);

		// Given a Mobile module
		$mobile = new Mobile($provider);

		// When we call the contact
		$actualCall = $mobile->makeCallByName($contactName);

		// Then a call should have been performed in the system
		$this->assertNotNull($actualCall);
		$this->assertInstanceOf(Call::class, $actualCall);
		$provider->shouldHaveReceived('dialContact')->with($contact);
		$provider->shouldHaveReceived('makeCall')->once();
		$this->assertEquals($expectedCall, $actualCall, 'Should have performed call');
		$this->assertEquals($contactName, $actualCall->getContact()->getName());
		$this->assertFalse($actualCall->isFailed());
	}

	/** @test */
	public function it_returns_null_when_calling_contact_not_found()
	{
		// Given a mobile carrier
		$provider = m::mock(CarrierInterface::class);

		// Given a Mobile module
		$mobile = new Mobile($provider);

		// When calling a contact not registered
		$call = $mobile->makeCallByName('Taylor Otwell');

		// Then the call should not be performed
		$this->assertNull($call, 'Should not perform call');
	}

	/** @test */
	public function it_returns_null_when_sending_sms_to_empty_number()
	{
		// Given a mobile carrier
		$provider = m::mock(CarrierInterface::class);

		// Given a Mobile module
		$mobile = new Mobile($provider);

		// When calling a contact not registered
		$sms = $mobile->sendSmsByNumber('', 'Message');

		// Then the call should not be performed
		$this->assertNull($sms, 'Should not send SMS');
	}

	/** @test */
	public function it_returns_null_when_sending_sms_to_empty_body()
	{
		// Given a mobile carrier
		$provider = m::mock(CarrierInterface::class);

		// Given a Mobile module
		$mobile = new Mobile($provider);

		// When sending sms with empty body
		$sms = $mobile->sendSmsByNumber('+51986821895', '');

		// Then the call should not be performed
		$this->assertNull($sms, 'Should not send SMS');
	}

	/** @test */
	public function it_returns_sms_instance_when_sending_sms_to_valid_phone_and_body()
	{
		// Given a phone number and a message
		$phoneNumber = '+51986821895';
		$message = 'Hi, there!';

		// Given a mobile carrier
		/**  @ReviewerNote */
		// In day to day testing I suggest using real testing implementations
		// instead of mocks, because it gives us more flexibility
		// to change method signatures using refactoring tools.
		// Whereas when using Mockery, changing a method name will potentially
		// break lots of tests making testing fragile instead of something
		// robust and useful.
		//
		// So for instance, the [Mobile] class needs an implementation of
		// [CarrierInterface]. In this case it's better to give it an actual
		// implementation such as [TestingCarrier] well prepared for testing.
		$provider = new TestingCarrier();
		$provider->withSuccessfulResponses();

		// Given a Mobile module
		$mobile = new Mobile($provider);

		// When sending message to the phone number
		$actualSms = $mobile->sendSmsByNumber($phoneNumber, $message);

		// Then a message instance should have been returned
		$this->assertNotNull($actualSms, 'Should send SMS');
		$this->assertInstanceOf(Sms::class, $actualSms);
		$this->assertEquals($phoneNumber, $actualSms->getPhoneNumber());
		$this->assertEquals($message, $actualSms->getMessage());
		$this->assertFalse($actualSms->isFailed());
	}

}
