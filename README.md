Chicho challenge
================

#### Use
1. Fork this repository
1. Run `composer install`
1. Run `php ./vendor/phpunit/phpunit/phpunit` (requires php >= 7.3 if you use other version please update composer.json file first)

#### Tasks

- Fix any errors and add the necessary to make the test work and commit it
- Add a test for the method makeCallByName passing a valid contact, mock up any hard dependency and add the right assertions
- Add the necessary code in the production code to check when the contact is not found and add another test to test that case
- Add your own logic to send a SMS given a number and the body, the method should validate the number using the validateNumber method from ContactService and using the provider property�s methods
- When writing the tests you should mock every method from ContactService

#### Bonus
- Can you add support for two mobile carriers? How would you accomplish that?


## Interviewee Notes
- I left comments where I found discussion points or needed to explain myself. They all start with `@ReviewerNote`.
- It's not clear whether the bonus task is literally asking for two implementations of the `CarrierInterface` or it is prompting for an approach using the Factory pattern.
  In any case, an Inversion of Control Container should need to be introduced in the equation to have a truly Open-Closed design.
- The `mockery/mockery` package should not go into the `require` section of the `composer.json` file. Since it's meant to be used on testing environments, it should go in the `require-dev` section.
- There are a lot of architectural decisions to be discussed.
- For instance, we could argue that the `Mobile` class has poor API design. It's initial method signature allows `null`s to be returned and also context objects. It will be very prone to error when a developer forgets to check `null` and whether the `Call` failed.

```php
    $call = $mobile->makeCallByName('Cristian Llanos');
    
    if (is_null($call)) {
        // Call was not made. And we don't exactly know why.
    }
    
    if ($call->isFailed()) {
        // Service unavailable. Call was not made.
    }
```

- It is way too easy for new developers to forget or not pay attention to this. If we did not allow `null` returns, it will have a better and clearer design.

```php
    $call = $mobile->makeCallByName('Cristian Llanos');

    if ($call->isFailed()) {
        // Service unavailable. Call was not made, but we know why.
        return $call->failureReason();
    }
    
    return 'Yay!';
```