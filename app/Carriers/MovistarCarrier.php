<?php


namespace App\Carriers;


use App\Call;
use App\Contact;
use App\Interfaces\CarrierInterface;
use App\Sms;
use Psr\Log\LoggerInterface;

class MovistarCarrier implements CarrierInterface
{

	private Contact $contact;

	/**
	 * @ReviewerNote
	 * A fictional SDK that allow us to make phone calls and
	 * send SMS.
	 *
	 * @var MovistarSdk
	 */
	private $movistarApi;

	/**
	 * @ReviewerNote
	 * Let's use PSR-3 Logger Interface (https://www.php-fig.org/psr/psr-3/)
	 * it'll give lot's of choices and great implementations.
	 *
	 * @var LoggerInterface
	 */
	private LoggerInterface $logger;

	public function __construct($movistarApi, LoggerInterface $logger)
	{
		$this->movistarApi = $movistarApi;
		$this->logger = $logger;
	}

	public function dialContact(Contact $contact)
	{
		$this->contact = $contact;
	}

	/**
	 * @ReviewerNote
	 * Since we don't know how the Movistar API works,
	 * we'll assume it returns a response object which
	 * exposes a set of methods such as [statusCode()]
	 * that will help us understand what happened to the request.
	 *
	 * In addition, we should set the [Call] status as well.
	 * As mentioned on another note, it might've been better to
	 * throw an exception, but we'll keep the design cause
	 * changing it might have a big impact on production code
	 * unless the task is to refactor the design.
	 *
	 * We'll assume everything went wrong and update if otherwise.
	 * It's better to think things will fail, because they will.
	 *
	 * @return Call
	 */
	public function makeCall(): Call
	{
		$call = new Call($this->contact);

		try {
			$response = $this->movistarApi->startCall(
				$this->contact->getMobilePhone(),
			);

			if ($response->statusCode() == 200) {
				$call->markAsSuccessful();
			}
		} catch (\Exception $e) {
			/**  @ReviewerNote */
			// We want to know when there was a programmatic error when using this SDK.
			// We'll log debug data to make our lives easier when fixing it and looking for clues.
			$this->logger->debug("Failed Movistar SDK when calling [{$this->contact->getName()}] at {$this->contact->getMobilePhone()}");
			$this->logger->error($e->getMessage(), [
				'trace' => $e->getTraceAsString()
			]);

			// We'll make sure it has a failed status. Better to be extra cautious.
			$call->markAsFailed();
		}

		return $call;
	}

	public function sendSms(string $phoneNumber, string $message): Sms
	{
		$sms = new Sms($phoneNumber, $message);

		try {
			$response = $this->movistarApi->sendSms(
				$phoneNumber,
				$message
			);

			if ($response->statusCode() == 200) {
				$sms->markAsSuccessful();
			}
		} catch (\Exception $e) {
			/**  @ReviewerNote */
			// We want to know when there was a programmatic error when using this SDK.
			// We'll log debug data to make our lives easier when fixing it and looking for clues.
			$this->logger->debug("Failed Movistar SDK when sending SMS to [$phoneNumber] - $message");
			$this->logger->error($e->getMessage(), [
				'trace' => $e->getTraceAsString()
			]);

			// We'll make sure it has a failed status. Better to be extra cautious.
			$sms->markAsFailed();
		}

		return $sms;
	}
}