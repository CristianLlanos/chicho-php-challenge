<?php


namespace App\Carriers;


use App\Call;
use App\Contact;
use App\Interfaces\CarrierInterface;
use App\Sms;

/**
 * Class SuccessfulTestingCarrier
 *
 * @ReviewerNote
 * This class is not meant to be in a production folder.
 * It might be better to put it within the testing folder
 * since it should never be used in production.
 *
 * @package App\Carriers
 */
class TestingCarrier implements CarrierInterface
{

	private Contact $contact;

	private bool $failed = false;

	public function withSuccessfulResponses() {
		$this->failed = false;
	}

	public function withFailedResponses() {
		$this->failed = true;
	}

	public function dialContact(Contact $contact)
	{
		$this->contact = $contact;
	}

	public function makeCall(): Call
	{
		$call = new Call($this->contact);

		if (!$this->failed) {
			$call->markAsSuccessful();
		}

		return $call;
	}

	public function sendSms(string $phoneNumber, string $message): Sms
	{
		$sms = new Sms($phoneNumber, $message);

		if (!$this->failed) {
			$sms->markAsSuccessful();
		}

		return $sms;
	}
}