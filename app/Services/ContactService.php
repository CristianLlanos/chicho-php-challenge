<?php

namespace App\Services;

use App\Contact;


class ContactService
{

	/**
	 * @ReviewerNote
	 * Let's keep an in-memory cache of our contacts to optimize processing time.
	 * It does not make a difference for an implementation like this one, but
	 * It does in production code.
	 *
	 * @var Contact[] In memory cache
	 */
	private static array $contacts = [];

	/**
	 * Fetch a contact by its name
	 *
	 * @param $name
	 * @return Contact|null
	 */
	public static function findByName($name): ?Contact
	{

		/**  @ReviewerNote */
		// Using PHP hashmaps is very fast, because it's searching on an index
		if (array_key_exists($name, static::$contacts)) {
			return static::$contacts[$name];
		}

		/**  @ReviewerNote */
		// Let's hard code this contact information since it's not possible to mock it
		// in our test files without changing the project design.
		if ($name == 'Cristian Llanos') {
			$contact = new Contact($name, '+51986821895');

			return static::$contacts[$name] = $contact;
		}

		return null;
	}

	/**
	 * Validates phone number format
	 *
	 * @param string $number
	 * @return bool
	 */
	public static function validateNumber(string $number): bool
	{
		/**  @ReviewerNote */
		// A phone number with at maximum 16 digits starting with plus sign
		// We could also rely on a third party package with phone validation
		// The open source community would probably keep track of more scenarios
		// And any new rule would be taken care by them. In this case, the
		// responsibility to keep this algorithm up to date is completely ours.

		return filter_var($number, FILTER_VALIDATE_REGEXP, [
			'options' => [
				'regexp' => '/^\+\d{8,16}/'
			]
		]) !== false;
	}
}