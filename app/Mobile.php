<?php

namespace App;

use App\Interfaces\CarrierInterface;
use App\Services\ContactService;


class Mobile
{

	protected CarrierInterface $provider;
	
	function __construct(CarrierInterface $provider)
	{
		$this->provider = $provider;
	}

	public function makeCallByName($name = ''): ?Call
	{
		if(empty($name)) {
			return null;
		}

		/**  @ReviewerNote */
		// Dependencies should never be hardcoded inside a class.
		// They should be passed through dependency injection or service location.
		// This kind of code makes testing more difficult and goes against isolation
		// once the production [ContactService] is implemented, because it will call
		// the data store for every single test that uses this method.
		// Whereas if this was a dependency, we could mock it and isolate our tests.
		// Moreover, this dependency should probably better be a repository because
		// "the service" actually is the Mobile class.
		$contact = ContactService::findByName($name);

		if (is_null($contact)) {
			return null;
		}

		/**  @ReviewerNote */
		// This line seems to function as a setter, even though the name suggests something different.
		// It is a very common mistake to give give a bad name to a function and confuse the reader.
		// In order to make our code cleaner we should give it a more appropriate name if this
		// method is needed at all.
		$this->provider->dialContact($contact);

		/**  @ReviewerNote */
		// And this method seems to do the actual call.
		// I would argue that a phone call cannot be made without a phone number (which belongs to a contact).
		// In this case I'd change the method signature to `makeCall($contact): Call` and get rid of `dialContact`.
		return $this->provider->makeCall();
	}

	/**
	 * @ReviewerNote
	 * Even though I'm following the API design from the method above by allowing null returns,
	 * I would suggest using exceptions, instead. APIs are more robust when we
	 * know what to expect and we make it week when we return nulls.
	 *
	 * In this case I would recommend using strict type return and throwing an
	 * exception, letting the system know that something very important such as
	 * sending an SMS did not happen due to some failure. Whereas when returning a
	 * null, it might give false positives if not implemented correctly or
	 * when null checks are missing.
	 *
	 * @param string $phoneNumber
	 * @param string $message
	 * @return Sms|null
	 */
	public function sendSmsByNumber(string $phoneNumber, string $message): ?Sms
	{
		if (!ContactService::validateNumber($phoneNumber)) {
			return null;
		}

		if (empty($message)) {
			return null;
		}

		/**  @ReviewerNote */
		// Unlike the method above, I'm not using setters, but executing the process.
		// When doing it this way, this method seems like a proxy with its only purpose
		// being validation (for now) and it's fine.
		//
		// The great thing about this approach is that this code is open-closed and thus
		// prepared to make validations that any [CarrierInterface] implementation would benefit from.
		// We will not repeat ourselves in the future.
		return $this->provider->sendSms($phoneNumber, $message);
	}


}
