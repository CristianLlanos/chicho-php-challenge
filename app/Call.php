<?php

namespace App;


class Call
{

	private Contact $contact;

	/**
	 * We'll assume all calls fail by default and change it when we are certain it didn't.
	 *
	 * @var bool
	 */
	private bool $failed = true;

	function __construct(Contact $contact)
	{
		$this->contact = $contact;
	}

	public function getContact(): Contact
	{
		return $this->contact;
	}

	public function markAsFailed()
	{
		$this->failed = true;
	}

	public function markAsSuccessful()
	{
		$this->failed = false;
	}

	public function isFailed(): bool
	{
		return $this->failed;
	}
}