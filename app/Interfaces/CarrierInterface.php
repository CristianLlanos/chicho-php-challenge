<?php

namespace App\Interfaces;

use App\Call;
use App\Sms;
use App\Contact;


interface CarrierInterface
{
	
	public function dialContact(Contact $contact);

	public function makeCall(): Call;

	public function sendSms(string $phoneNumber, string $message): Sms;
}