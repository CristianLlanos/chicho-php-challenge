<?php

namespace App;


class Contact
{

	private string $name;
	private string $mobilePhone;

	function __construct(string $name, string $mobilePhone)
	{
		$this->name = $name;
		$this->mobilePhone = $mobilePhone;
	}

	public function getName(): string
	{
		return $this->name;
	}

	public function getMobilePhone(): string
	{
		return $this->mobilePhone;
	}
}