<?php


namespace App;


class Sms
{
	private string $phoneNumber;
	private string $message;

	/**
	 * We'll assume sending an SMS fails by default until we are certain it did not.
	 *
	 * @var bool
	 */
	private bool $failed = true;

	/**
	 * Sms constructor.
	 */
	public function __construct(string $phoneNumber, string $message)
	{
		$this->phoneNumber = $phoneNumber;
		$this->message = $message;
	}

	public function getPhoneNumber(): string
	{
		return $this->phoneNumber;
	}

	public function getMessage(): string
	{
		return $this->message;
	}

	public function markAsFailed()
	{
		$this->failed = true;
	}

	public function markAsSuccessful()
	{
		$this->failed = false;
	}

	public function isFailed(): bool
	{
		return $this->failed;
	}
}